import './App.css';
import { BrowserRouter as Router, withRouter, Route, Switch } from "react-router-dom";
import Fitur from './component/Fitur';
import Login from './component/Login';
import TableDosen from './component/TableDosen';
import TableRiwayat from './component/TableRiwayat';
import CreateDosen from './component/CreateDosen';
import CreateRiwayat from './component/CreateRiwayat';
import CreateMahasiswa from './component/CreateMahasiswa';
import CreateMataKuliah from './component/CreateMataKuliah';
import CreateKelas from './component/CreateKelas';
import IndexKelas from './component/IndexKelas';
import { store } from './app/store'
import { Provider } from 'react-redux'

function App() {
  return (
    <Router>
      <Provider store={store}>
        <Switch>
          <Route path="/" exact component={Fitur} />
          <Route path="/login" exact component={Login}/>
          <Route path="/index/dosen" exact component={TableDosen} />
          <Route path="/index/mhs" exact component={TableDosen} />
          <Route path="/index/matakuliah" exact component={TableDosen} />
          <Route path="/add/dosen" exact component={CreateDosen} />
          <Route path="/edit/dosen" exact component={CreateDosen} />
          <Route path="/index/riwayat" exact component={TableRiwayat} />
          <Route path="/add/rpend" exact component={CreateRiwayat} />
          <Route path="/edit/rpend" component={CreateRiwayat} />
          <Route path="/details/mhs" exact component={IndexKelas} />
          <Route path="/add/mhs" exact component={CreateMahasiswa} />
          <Route path="/edit/mhs" exact component={CreateMahasiswa} />
          <Route path="/index/mk" exact component={TableDosen} />
          <Route path="/add/mk" exact component={CreateMataKuliah} />
          <Route path="/edit/mk" exact component={CreateMataKuliah} />
          <Route path="/add/kelas" exact component={CreateKelas} />
        </Switch>
      </Provider>
    </Router>
  );
}

export default App;
