import React, { useEffect, useState} from 'react';
import List from './List';
import { Link, useLocation } from "react-router-dom";
import '../css/TableDosen.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }

const TableDosen = () => {
    let daerah = useLocation();
    const [data, setData] = useState([]);
    const [column, setColumn] = useState([]);
    const [url, setUrl] = useState('');
    const [idFitur] = useState(daerah.idPage);
    const [namaJudul, setNamaJudul] = useState('');
    const token = getLocalStorage();

    console.log(token);

    const axios = require('axios').default;

    useEffect(() => {
        if(!token) {
            alert('Anda belum login!');
        }
        if(idFitur===1){
            setColumn(['id', 'nama', 'NIP', 'gelar', 'aksi']);
            setUrl('/dosen');
            setNamaJudul('Dosen');
        } else if(idFitur===2) {
            setColumn(['nama', 'NIM', 'jenis_kelamin', 'TTL', 'aksi']);
            setUrl('/mhs');
            setNamaJudul('Mahasiswa');
        } else {
            setColumn(['nama', 'sks', 'aksi']);
            setUrl('/mk');
            setNamaJudul('Mata Kuliah');
        }
    }, []);

    useEffect(() => {
        async function fetchMyAPI() {
            axios.get('/api'+url,{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response => {
                if(response.status!==200) {
                    throw new Error(response.statusText);
                }
                return response;
            })
            .then(response => {
                if(response.status !== 200) {
                    throw new Error(response.Error);
                }
                setData(response.data['data']);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        if(url) {
            fetchMyAPI();
        }
    }, [url]);

    const handleDelete = (id) => {
        fetch(`/api${url}/${id}`, {
            method: 'DELETE'
        })
        .catch(error => {
            console.log("Error fetching data: ", error);
        });
    }

    return (
        <div className="daerah-card">
            <h1>Daftar {namaJudul}</h1>
            <table>
                <thead>
                    <tr>
                        {
                            column.map((item, index) => {
                                return <th key={index}>{item}</th>
                            })
                        }
                    </tr>
                </thead>
                <tbody>
                    {
                        data.length > 0 &&
                        <List data={data} idFitur={idFitur} removeItem={handleDelete} column={column.slice(0, column.length-1)}/>
                    }
                </tbody>
            </table>
            <div className="btn-wrapper">
                <Link to={{
                    pathname: "/add"+url
                }}>
                    <button className="btn btn-add">Tambah</button>
                </Link>
                <Link to='/'><button className="btn btn-back">Back</button></Link>
                {/* {
                    (idFitur===3) &&
                    <Link to='/daftarMkDosen'><button className="btn btn-detail">Detail Mata Kuliah</button></Link>
                } */}
            </div>
        </div>
    );
}

export default TableDosen;