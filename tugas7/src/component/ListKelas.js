import React from 'react';
import { Link } from "react-router-dom";
// import '../css/ListDaerah.css';

const List = ({data, removeItem}) => {
    return (
        data.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{item.pivot['nama_kelas']}</td>
                    <td>{item['nama']}</td>
                    <td>{item.pivot['id_mahasiswa']}</td>
                    <td>{item['sks']}</td>
                    <td>
                        <button onClick={() => removeItem(item.pivot['id_mahasiswa'], item.pivot['id_matkul'])} className="btn btn-delete">delete</button>
                    </td>
                </tr>
            );
        })
    );
}

export default List;