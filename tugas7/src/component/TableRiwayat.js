import React, { useEffect, useState} from 'react';
import ListRiwayat from './ListRiwayat';
import { Link, useLocation } from "react-router-dom";
// import '../css/TableDaerah.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }

const TableRiwayat = () => {
    const [data, setData] = useState([]);
    const token = getLocalStorage();
    const [listIdDosen, setListIdDosen] = useState([]);

    const axios = require('axios').default;

    useEffect(() => {
        if(!token) {
            alert('Anda belum login!');
        }
        async function fetchMyAPI() {
            await axios.get('/api/dosen',{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response => {
                if(response.status!==200) {
                    throw new Error(response.statusText);
                }
                return response;
            })
            .then(response => {
                if(response.status !== 200) {
                    throw new Error(response.Error);
                }
                setListIdDosen(response.data.data);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        fetchMyAPI();
    }, []);


    useEffect(() => {
        async function fetchMyAPI() {
            axios.get('/api/rpend',{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response => {
                if(response.status!==200) {
                    throw new Error(response.statusText);
                }
                return response;
            })
            .then(response => {
                if(response.status !== 200) {
                    throw new Error(response.Error);
                }
                setData(response.data['data']);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        fetchMyAPI();
    }, []);

    const handleDelete = (id) => {
        fetch(`/api/rpend/${id}`, {
            method: 'DELETE'
        })
        .catch(error => {
            console.log("Error fetching data: ", error);
        });
    }

    return (
        <div className="daerah-card">
            <h1>Daftar Riwayat Pendidikan</h1>
            {
                data.length > 0 ? 
                <>
                <table>
                    <thead>
                        <tr>
                            <th>ID Dosen</th>
                            <th>Strata</th>
                            <th>Jurusan</th>
                            <th>Sekolah</th>
                            <th>Tahun Mulai</th>
                            <th>Tahun Selesai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.length > 0 &&
                            <ListRiwayat data={data} listIdDosen={listIdDosen} removeItem={handleDelete}/>
                        }
                    </tbody>
                </table>
                <div className="btn-wrapper">
                    <Link to={{
                        pathname: "/add/rpend",
                        listIdDosen: listIdDosen
                    }}>
                        <button className="btn btn-add">Tambah</button>
                    </Link>
                    <Link to='/'><button className="btn btn-back">Back</button></Link>
                </div>
                </>
                :
                "Tidak ada kelas"
            }
        </div>
    );
}

export default TableRiwayat;