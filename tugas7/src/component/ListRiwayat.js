import React from 'react';
import { Link } from "react-router-dom";
// import '../css/ListDaerah.css';

const List = ({data, removeItem, listIdDosen}) => {
    return (
        data.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{item['id_dosen']}</td>
                    <td>{item['strata']}</td>
                    <td>{item['jurusan']}</td>
                    <td>{item['sekolah']}</td>
                    <td>{item['tahun_mulai']}</td>
                    <td>{item['tahun_selesai']}</td>
                    <td>
                        <Link to={{
                            pathname: "/edit/rpend",
                            id: item.id,
                            idDosenEdit: item.id_dosen,
                            strataEdit: item.strata,
                            jurusanEdit: item.jurusan,
                            sekolahEdit: item.sekolah,
                            tahunMulaiEdit: item.tahun_mulai,
                            tahunSelesaiEdit: item.tahun_selesai,
                            isEditingEdit: true,
                            listIdDosen: listIdDosen
                        }}> 
                            <button className="btn btn-edit">edit</button>
                        </Link>
                        <button onClick={() => removeItem(item.id)} className="btn btn-delete">delete</button>
                    </td>
                </tr>
            );
        })
    );
}

export default List;