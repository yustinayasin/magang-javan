import React, { useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { Link } from "react-router-dom";
// import '../css/CreateDaerah.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }
  
const CreateMataKuliah = () => {
    let edit = useLocation();
    const [nama, setNama] = useState('');
    const [SKS, setSKS] = useState('');
    const [isEditing, setIsEditing] = useState(edit["isEditingEdit"]);
    const [id] = useState(edit["id"]);
    const token = getLocalStorage();
    const axios = require('axios').default;

    useEffect(() => {
        if(edit.namaEdit && isEditing) {
            let newNama = edit.namaEdit;
            setNama(newNama);
        }  
        if(edit.SKSEdit && isEditing) {
            let newSKS = edit.NIMEdit;
            setSKS(newSKS);
        }  
    }, []);


    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(nama);
        console.log(SKS);
        axios({
            method: 'post',
            url: '/api/mk',
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                nama: nama,
                sks: SKS
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err.response.data));
    }

    const handleEdit = (id) => {
        axios({
            method: 'put',
            url: `/api/mk/${id}`,
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                nama: nama,
                sks: SKS,
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
        setIsEditing(false);
    }

    return(
        <div className="card-create">
            <form onSubmit={isEditing ? () => handleEdit(id) : (e) => handleSubmit(e)}>
                <h1>Form Mata Kuliah</h1>
                <label htmlFor="nama">Nama</label>
                <input type="text" 
                        className="nama" 
                        name="nama"
                        defaultValue={isEditing && edit["namaEdit"]}
                        placeholder="masukkan nama"
                        onChange={(e) => setNama(e.target.value)}
                />
                <label htmlFor="SKS">SKS</label>
                <input type="text" 
                        className="SKS" 
                        name="SKS"
                        defaultValue={isEditing && edit["SKSEdit"]}
                        placeholder="masukkan SKS"
                        onChange={(e) => setSKS(e.target.value)}
                />
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">{isEditing ? 'Edit' : 'Submit'}</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateMataKuliah;