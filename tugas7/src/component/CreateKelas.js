import React, { useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { Link } from "react-router-dom";
// import '../css/CreateDaerah.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }
  
const CreateKelas = () => {
    const mhs = useLocation();
    const [namaKelas, setNamaKelas] = useState('');
    const [idMatkul, setIDMatkul] = useState('');
    const token = getLocalStorage();
    const [sks, setSKS] = useState('');

    const axios = require('axios').default;

    const handleSubmit = (e) => {
        e.preventDefault();
        if((24-mhs.jumlahSKS)>sks || (24-mhs.jumlahSKS)===sks ) {
            axios({
                method: 'post',
                url: '/api/submit-kelas',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                data: {
                    nama_kelas: namaKelas,
                    id_matkul: idMatkul,
                    id_mhs: mhs.idMhs
                }
            }).then(() => alert('Data berhasil ditambahkan!'))
            .catch(err => console.error(err.response.data));
        } else {
            alert('Jumlah sks tidak mencukupi!')
        }
    }

    const onChange = (e) => {
        setIDMatkul(e.target.value);
        setSKS(e.target.sks);
    }

    return(
        <div className="card-create">
            <form onSubmit={(e) => handleSubmit(e)}>
                <h1>Form Kelas</h1>
                <label htmlFor="namaKelas">Nama Kelas</label>
                <input type="text" 
                        className="namaKelas" 
                        name="namaKelas"
                        placeholder="masukkan nama kelas"
                        onChange={(e) => setNamaKelas(e.target.value)}
                />
                <label htmlFor="idMatkul">ID Mata Kuliah</label>
                <select name="idMatkul" className="idMatkul" onChange={(e) => onChange(e)}>
                    {
                        mhs.listIdMatkul ? 
                        <>
                            {
                                mhs.listIdMatkul.map((item, index) => {
                                    return <option key={index} data-sks={item.sks} value={item.id}>{item.nama}</option>
                                })
                            }
                        </>
                        :
                        "Tidak ada mata kuliah"
                    }
                </select>
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">Submit</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateKelas;