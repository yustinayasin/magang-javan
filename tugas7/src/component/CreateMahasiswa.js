import React, { useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { Link } from "react-router-dom";
// import '../css/CreateDaerah.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }
  
const CreateMahasiswa = () => {
    let edit = useLocation();
    const [nama, setNama] = useState('');
    const [NIM, setNIM] = useState('');
    const [jenisKelamin, setJenisKelamin] = useState('');
    const [ttl, setTtl] = useState('');
    const [isEditing, setIsEditing] = useState(edit["isEditingEdit"]);
    const [id] = useState(edit["id"]);
    const token = getLocalStorage();

    const axios = require('axios').default;

    const handleSubmit = () => {
        axios({
            method: 'post',
            url: '/api/mhs',
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                nama: nama,
                NIM: NIM,
                jenis_kelamin: jenisKelamin,
                TTL: ttl
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
    }

    const handleEdit = (id) => {
        axios({
            method: 'put',
            url: `/api/mhs/${id}`,
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                nama: nama,
                NIM: NIM,
                jenis_kelamin: jenisKelamin,
                TTL: ttl
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
        setIsEditing(false);
    }

    return(
        <div className="card-create">
            <form onSubmit={isEditing ? () => handleEdit(id) : () => handleSubmit()}>
                <h1>Form Mahasiswa</h1>
                <label htmlFor="nama">Nama</label>
                <input type="text" 
                        className="nama" 
                        name="nama"
                        defaultValue={isEditing && edit["namaEdit"]}
                        placeholder="masukkan nama"
                        onChange={(e) => setNama(e.target.value)}
                />
                <label htmlFor="NIM">NIM</label>
                <input type="text" 
                        className="NIM" 
                        name="NIM"
                        defaultValue={isEditing && edit["NIMEdit"]}
                        placeholder="masukkan NIM"
                        onChange={(e) => setNIM(e.target.value)}
                />
                <label htmlFor="jenisKelamin">Jenis Kelamin</label>
                <input type="text" 
                        className="jenisKelamin" 
                        name="jenisKelamin"
                        defaultValue={isEditing && edit["jenisKelaminEdit"]}
                        placeholder="masukkan jenis kelamin"
                        onChange={(e) => setJenisKelamin(e.target.value)}
                />
                <label htmlFor="ttl">Tanggal Lahir</label>
                <input type="date" 
                        className="ttl" 
                        name="ttl"
                        defaultValue={isEditing && new Date(edit["ttlEdit"])}
                        onChange={(e) => setTtl(e.target.value.toString())}
                />
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">{isEditing ? 'Edit' : 'Submit'}</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateMahasiswa;