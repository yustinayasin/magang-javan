import React, {useState} from 'react';
import { Link } from "react-router-dom";
import '../css/Fitur.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }

const Fitur = () => {
    const token = getLocalStorage();

    const handleLogout = () => {
        localStorage.setItem('token', JSON.stringify(''));
    }

    return (
        <div className="fitur">
            <nav>
                {
                    token ?
                    <>
                        <Link to={{
                            pathname: "/"
                        }}>
                            <button className="btn btn-login" onClick={handleLogout}>Logout</button>
                        </Link>
                    </> :
                    <>
                        <Link to={{
                            pathname: "/login"
                        }}>
                            <button className="btn btn-login">Login</button>
                        </Link>
                </>
                }
            </nav>
            <Link to={{
                pathname: "/index/dosen",
                idPage: 1
            }}>
                <button className="btn btn-dosen">Dosen</button>
            </Link>
            <Link to={{
                pathname: "/index/riwayat",
            }}>
                <button className="btn btn-riwayat">Riwayat Pendidikan</button>
            </Link>
            <Link to={{
                pathname: "/index/mhs",
                idPage: 2
            }}>
                <button className="btn btn-mahasiswa">Mahasiswa</button>
            </Link>
            <Link to={{
                pathname: "/index/mk",
                idPage: 3
            }}>
                <button className="btn btn-matakuliah">Mata Kuliah</button>
            </Link>
        </div>
    );
}

export default Fitur;