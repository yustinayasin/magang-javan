import React, { useEffect, useState} from 'react';
import ListKelas from './ListKelas';
import { Link, useLocation } from "react-router-dom";
// import '../css/TableDaerah.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }

const IndexKelas = () => {
    const mhs = useLocation();
    const [data, setData] = useState([]);
    const [jumlahSKS, setJumlahSKS] = useState(null);
    const token = getLocalStorage();
    const [listIdMatkul, setListIdMatkul] = useState([]);

    const axios = require('axios').default;

    useEffect(() => {
        async function fetchMyAPI() {
            await axios.get('/api/mk',{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response => {
                if(response.status!==200) {
                    throw new Error(response.statusText);
                }
                return response;
            })
            .then(response => {
                if(response.status !== 200) {
                    throw new Error(response.Error);
                }
                setListIdMatkul(response.data.data);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        fetchMyAPI();
    }, []);

    console.log(listIdMatkul);

    useEffect(() => {
        setJumlahSKS(null);
        async function fetchMyAPI() {
            await axios.get(`/api/mhs/${mhs.id}`,{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response => {
                if(response.status!==200) {
                    throw new Error(response.statusText);
                }
                return response;
            })
            .then(response => {
                if(response.status !== 200) {
                    throw new Error(response.Error);
                }
                setData(response.data['kelas']);
                hitungJumlahSKS(data);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        fetchMyAPI();
    }, []);

    const handleDelete = async (idMhs, idMatkul) => {
        await axios({
            method: 'post',
            url: '/api/unsubmit-kelas',
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                'id_mhs': idMhs,
                'id_matkul': idMatkul
            }
        })
        .catch(error => {
            console.log("Error fetching data: ", error.response.data);
        });
    }

    const hitungJumlahSKS = (data) => {
        data.forEach((item) => {
            setJumlahSKS(jumlahSKS => jumlahSKS+item.sks);
        })
        return jumlahSKS;
    }

    return (
        <div className="daerah-card">
            <h1>Detail Kelas</h1>
            {
                data.length > 0 ? 
                <>
                <table>
                    <thead>
                        <tr>
                            <th>Nama Kelas</th>
                            <th>Mata Kuliah</th>
                            <th>ID Mahasiswa</th>
                            <th>SKS</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.length > 0 &&
                            <ListKelas data={data} removeItem={handleDelete}/>
                        }
                    </tbody>
                </table>
                <p>Jumlah sks : {jumlahSKS}</p>
                <div className="btn-wrapper">
                    <Link to={{
                        pathname: "/add/kelas",
                        idMhs: mhs.id,
                        listIdMatkul: listIdMatkul,
                        jumlahSKS: jumlahSKS
                    }}>
                        <button className="btn btn-add">Tambah</button>
                    </Link>
                    <Link to='/'><button className="btn btn-back">Home</button></Link>
                </div>
                </>
                :
                "Tidak ada kelas"
            }
        </div>
    );
}

export default IndexKelas;