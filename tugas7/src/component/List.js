import React from 'react';
import { Link } from "react-router-dom";
import '../css/List.css';

const List = ({data, removeItem, column, idFitur}) => {
    const checkEdit = (idFitur, item) => {
        if(idFitur===1) {
            return (
                <>
                <Link to={{
                pathname: "/edit/dosen",
                id: item.id,
                namaEdit: item.nama,
                NIPEdit: item.NIP,
                gelarEdit: item.gelar,
                isEditingEdit: true,
                }}> 
                    <button className="btn btn-edit">edit</button>
                </Link>
                </>
            );
        } else if(idFitur===2) {
            return (
                <>
                <Link to={{
                pathname: "/details/mhs",
                id: item.id
                }}> 
                    <button className="btn btn-details">details</button>
                </Link>
                <Link to={{
                    pathname: "/edit/mhs",
                    id: item.id,
                    namaEdit: item.nama,
                    NIMEdit: item.NIM,
                    jenisKelaminEdit: item.jenis_kelamin,
                    ttlEdit: item.TTL,
                    isEditingEdit: true,
                }}> <button className="btn btn-edit">edit</button>
                </Link>
                </>
            )
        } else {
            return <Link to={{
                pathname: "/edit/mk",
                id: item.id,
                namaEdit: item.nama,
                SKSEdit: item.sks,
                isEditingEdit: true,
            }}> <button className="btn btn-edit">edit</button>
            </Link>
        }
    }
    return(
        <>
        {
            data.map((item, index) => {
                return (
                    <tr key={index}>
                        {
                            column.map((item2, index2) => {
                                return <td key={index2}>{item[item2]}</td>
                            })
                        }
                        <td>
                            {
                                checkEdit(idFitur, item)
                            }
                            <button onClick={() => removeItem(item.id)} className="btn btn-delete">delete</button>
                        </td>
                    </tr>
                );
            })
        }
        </>
    );
}

export default List;