import React, { useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { Link } from "react-router-dom";
import '../css/CreateDosen.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }

const CreateDosen = () => {
    let edit = useLocation();
    const [nama, setNama] = useState('');
    const [NIP, setNIP] = useState('');
    const [gelar, setGelar] = useState('');
    const [isEditing, setIsEditing] = useState(edit["isEditingEdit"]);
    const [id] = useState(edit["id"]);
    const token = getLocalStorage();


    const axios = require('axios').default;
    
    const handleSubmit = () => {
        axios({
            method: 'post',
            url: '/api/dosen',
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                nama: nama,
                NIP: NIP,
                gelar: gelar
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
    }

    const handleEdit = (id, e) => {
        e.preventDefault();
        axios({
            method: 'put',
            url: `/api/dosen/${id}`,
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                nama: nama,
                NIP: NIP,
                gelar: gelar
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err.response.data));
        setIsEditing(false);
        setNama('');
        setNIP('');
        setGelar('');
    }

    return(
        <div className="card-create">
            <form onSubmit={isEditing ? (e) => handleEdit(id, e) : () => handleSubmit()}>
                <h1>Form Dosen</h1>
                <label htmlFor="nama">Nama:</label>
                <input type="text" 
                        className="nama" 
                        name="nama"
                        defaultValue={isEditing && edit["namaEdit"]}
                        placeholder="masukkan nama"
                        onChange={(e) => setNama(e.target.value)}
                />
                <label htmlFor="NIP">NIP:</label>
                <input type="text" 
                        className="NIP" 
                        name="NIP"
                        defaultValue={isEditing && edit["NIPEdit"]}
                        placeholder="masukkan NIP"
                        onChange={(e) => setNIP(e.target.value)}
                />
                <label htmlFor="gelar">Gelar:</label>
                <input type="text" 
                        className="gelar" 
                        name="gelar"
                        defaultValue={isEditing && edit["gelarEdit"]}
                        placeholder="masukkan gelar"
                        onChange={(e) => setGelar(e.target.value)}
                />
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">{isEditing ? 'Edit' : 'Submit'}</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateDosen;