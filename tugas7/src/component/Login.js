import React, { useState, useEffect } from 'react';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [token, setToken] = useState(getLocalStorage());

    const axios = require('axios').default;

    useEffect(() => {
        localStorage.setItem('token', JSON.stringify(token));
      }, [token]);

    const handleSubmit = (e) => {
        e.preventDefault();
        axios.post('/api/login',{
            "email": email,
            "password": password
        }).then(response => {
            setToken(response['data']['content']['access_token']);
        })
        .catch(error => console.error(error));
    }

    return (
        <div className="card-create">
            <h1>Login</h1>
            <form onSubmit={handleSubmit}>
                <label htmlFor="email">Email:</label>
                <input type="text" name="email" placeholder="Masukkan email" onChange={(e) => setEmail(e.target.value)}/>
                <label htmlFor="password">Password:</label>
                <input type="password"  name="password" placeholder="Masukkan password" onChange={(e) => setPassword(e.target.value)}/>
                <div className="btn-wrapper">
                  <button className="btn btn-add" type="submit">submit</button>
                </div>
            </form>
        </div>
    );
}

export default Login;