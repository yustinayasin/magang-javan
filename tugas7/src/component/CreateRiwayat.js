import React, { useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { Link } from "react-router-dom";
// import '../css/CreateDaerah.css';

const getLocalStorage = () => {
    let list = localStorage.getItem('token');
    if(list) {
      return JSON.parse(localStorage.getItem('token'));
    } else {
      return '';
    }
  }
  
const CreateRiwayat = () => {
    let dosen = useLocation();
    const [id] = useState(dosen["id"]);
    const [idDosen, setIdDosen] = useState('');
    const [strata, setStrata] = useState('');
    const [jurusan, setJurusan] = useState('');
    const [sekolah, setSekolah] = useState('');
    const [tahunMulai, setTahunMulai] = useState('');
    const [tahunSelesai, setTahunSelesai] = useState('');
    const [isEditing, setIsEditing] = useState(dosen["isEditingEdit"]);
    const token = getLocalStorage();

    const axios = require('axios').default;

    const handleSubmit = () => {
        axios({
            method: 'post',
            url: '/api/rpend',
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                id_dosen: idDosen,
                strata: strata,
                jurusan: jurusan,
                sekolah: sekolah,
                tahun_mulai: tahunMulai,
                tahun_selesai: tahunSelesai
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
    }

    const handleEdit = (id) => {
        axios({
            method: 'put',
            url: `/api/mhs/${id}`,
            headers: {
                'Authorization': `Bearer ${token}`
            },
            data: {
                id_dosen: idDosen,
                strata: strata,
                jurusan: jurusan,
                sekolah: sekolah,
                tahun_mulai: tahunMulai,
                tahun_selesai: tahunSelesai
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
        setIsEditing(false);
    }

    return(
        <div className="card-create">
            <form onSubmit={isEditing ? () => handleEdit(id) : () => handleSubmit()}>
                <h1>Form Riwayat</h1>
                <label htmlFor="idDosen">Nama Dosen</label>
                <select name="idDosen" className="idDosen" defaultValue={dosen.idDosenEdit} onChange={(e) => setIdDosen(e.target.value)}>
                    {
                        dosen.listIdDosen ? 
                        <>
                            {
                                dosen.listIdDosen.map((item, index) => {
                                    return <option key={index} value={item.id}>{item.nama}</option>
                                })
                            }
                        </>
                        :
                        "Tidak ada dosen"
                    }
                </select>
                <label htmlFor="strata">Strata</label>
                <input type="text" 
                        className="strata" 
                        name="strata"
                        defaultValue={isEditing && dosen["strataEdit"]}
                        placeholder="masukkan strata"
                        onChange={(e) => setStrata(e.target.value)}
                />
                <label htmlFor="jurusan">Jurusan</label>
                <input type="text" 
                        className="jurusan" 
                        name="jurusan"
                        defaultValue={isEditing && dosen["jurusanEdit"]}
                        placeholder="masukkan jurusan"
                        onChange={(e) => setJurusan(e.target.value)}
                />
                <label htmlFor="sekolah">Sekolah</label>
                <input type="text" 
                        className="sekolah" 
                        name="sekolah"
                        defaultValue={isEditing && dosen["sekolahEdit"]}
                        placeholder="masukkan sekolah"
                        onChange={(e) => setSekolah(e.target.value)}
                />
                <label htmlFor="tahunMulai">Tahun Mulai</label>
                <input type="number" 
                        className="tahunMulai" 
                        name="tahunMulai"
                        min="1900" 
                        max="2099" 
                        step="1"
                        defaultValue={isEditing && dosen["tahunMulaiEdit"]}
                        onChange={(e) => setTahunMulai(e.target.value.toString())}
                />
                <label htmlFor="tahunSelesai">Tahun Selesai</label>
                <input type="number" 
                        className="tahunSelesai" 
                        name="tahunSelesai"
                        min="1900" 
                        max="2099" 
                        step="1"
                        defaultValue={isEditing && dosen["tahunSelesaiEdit"]}
                        onChange={(e) => setTahunSelesai(e.target.value.toString())}
                />
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">{isEditing ? 'Edit' : 'Submit'}</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateRiwayat;