import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  token: ''
}

const tokenSlice = createSlice({
  name: 'token',
  initialState,
  reducers: {
    setToken: (state, action) => {
      console.log('jahaah');
      return {
        ...state, 
        token: action.payload.token};
    }
  },
})


export const { setToken } = tokenSlice.actions

export default tokenSlice.reducer