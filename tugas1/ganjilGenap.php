<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ganjil Genap</title>
</head>

<body>
    <?php
    function ganjilGenap($a, $b)
    {
        echo "<br>";
        for ($i = $a; $i <= $b; $i++) {
            if ($i % 2 === 0) {
                echo "Angka " . $i . " adalah genap <br>";
            } else {
                echo "Angka " . $i . " adalah ganjil <br>";
            }
        }
    }

    ganjilGenap(1, 4);
    ganjilGenap(6, 8);
    ?>
</body>

</html>