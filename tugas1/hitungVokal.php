<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hitung Vokal</title>
</head>

<body>
    <?php
    function hitungVokal($kata)
    {
        $vokal = '@[aeiouAIUEO]@';
        preg_match_all($vokal, $kata, $matches);
        $unique = array_unique($matches[0]);

        print($kata . ": ");
        printf('Ada %d huruf vokal diantaranya ', count($unique));
        for ($i = 0; $i < count($unique); $i++) {
            if ($i == count($unique) - 1 && count($unique) > 1) {
                echo " dan " . $unique[$i];
            }
            if (count($unique) == 1) {
                echo $unique[$i];
            }
            if ($i <= count($unique) - 2) {
                echo $unique[$i] . ", ";
            }
        }
        echo "<br>";
    }

    hitungVokal("hahahaa");
    hitungVokal("yustina yasin");
    ?>
</body>

</html>