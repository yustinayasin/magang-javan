<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ganjil Genap</title>
</head>

<body>
    <?php
    function kalkulator($perintah)
    {
        $perintah2 = explode(" ", $perintah);
        $number1 = (int)$perintah2[0];
        $number2 = (int)$perintah2[2];

        switch ($perintah2[1]) {
            case "+":
                $output = $number1 + $number2;
                break;
            case "-":
                $output = $number1 - $number2;
                break;
            case "*":
                $output = $number1 * $number2;
                break;
            case "/":
                if ($number2 === 0) {
                    $output = "Tidak bisa dilakukan";
                    break;
                }
                $output = $number1 / $number2;
                break;
        }

        return print($output . "<br>");
    }

    kalkulator("2 + 2");
    kalkulator("3 * 2");
    ?>
</body>

</html>