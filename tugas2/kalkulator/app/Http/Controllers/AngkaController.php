<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AngkaController extends Controller
{
    //
    public function operasi(Request $request)
    {
        if (isset($request->operasi)) {
            $a = $request->angka1;
            $b = $request->angka2;
            $op = $request->operasi;
        }

        if (is_numeric($a) && is_numeric(($b))) {
            switch ($op) {
                case 'tambah':
                    $hasil = $a + $b;
                    break;
                case 'kurang':
                    $hasil = $a - $b;
                    break;
                case 'kali':
                    $hasil = $a * $b;
                    break;
                case 'bagi':
                    if ($b == 0) {
                        $hasil = "Tidak bisa dilakukan";
                    } else {
                        $hasil = $a / $b;
                    }
                    break;
            }
        }

        return View::make("tampilkan", compact('hasil'));
    }
}
