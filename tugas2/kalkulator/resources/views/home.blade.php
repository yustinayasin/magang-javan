<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kalkulator</title>
    <link rel="stylesheet" type="text/css" href="/css/home.css">
</head>

<body>
    <form action="/hasil" method="post">
        {{ csrf_field() }}
        <h1>Kalkulator</h1>
        <div class="angka">
            <label for="angka1">Angka 1:</label>
            <input type="number" name="angka1" id="angka1">
        </div>

        <div class="angka">
            <label for="angka2">Angka 1:</label>
            <input type="number" name="angka2" id="angka2">
        </div>

        <div>
            <button type="submit" value="tambah" name="operasi">tambah</button>
            <button type="submit" value="kurang" name="operasi">kurang</button>
            <button type="submit" value="kali" name="operasi">kali</button>
            <button type="submit" value="bagi" name="operasi">bagi</button>
        </div>
    </form>
</body>

</html>