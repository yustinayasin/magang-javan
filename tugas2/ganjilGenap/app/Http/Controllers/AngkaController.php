<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AngkaController extends Controller
{
    //
    public function kalkulasi(Request $request)
    {
        $angka1 = $request->angka1;
        $angka2 = $request->angka2;
        $array = [];

        for ($i = $angka1; $i <= $angka2; $i++) {
            if ($i % 2 === 0) {
                $output = "Genap";
            } else {
                $output = "Ganjil";
            }
            array_push($array, [$i, $output]);
        }

        //return view('hasilGanjilGenap', $array);
        return View::make("hasilGanjilGenap", compact('array'));
        //return view('hasilGanjilGenap')->with($data);
        //return view('hasilGanjilGenap');
    }
}
