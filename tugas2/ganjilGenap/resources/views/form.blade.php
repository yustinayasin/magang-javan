<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Ganjil Genap</title>
    <link rel="stylesheet" type="text/css" href="/css/form.css">
</head>

<body>
    <form action="/hasil/kalkulator" method="POST" id="main-form">
        {{ csrf_field() }}
        <h1>Form Ganjil Genap</h1>
        <label for="angka1">Angka pertama:</label>
        <input type="text" name="angka1" id="angka1" placeholder="Masukkan angka pertama">
        <label for="angka2">Angka kedua:</label>
        <input type="text" name="angka2" id="angka2" placeholder="Masukkan angka kedua">
        <button type="submit">SUBMIT</button>
    </form>
</body>

</html>