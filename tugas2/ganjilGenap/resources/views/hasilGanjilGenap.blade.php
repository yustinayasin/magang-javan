<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Hasil</title>
    <link rel="stylesheet" type="text/css" href="/css/tabel.css">
</head>

<body>
    <h1>Tabel Hasil Ganjil Genap</h1>
    <table>
        <tr>
            <th>Angka</th>
            <th>Ganjil/Genap</th>
        </tr>
        @foreach($array as $p)
        <tr>
            <td>{{ $p[0] }}</td>
            <td>{{ $p[1] }}</td>
        </tr>
        @endforeach
    </table>
</body>

</html>