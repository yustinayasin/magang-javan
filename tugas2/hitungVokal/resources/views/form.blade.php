<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Ganjil Genap</title>
    <link rel="stylesheet" type="text/css" href="/css/form.css">
</head>

<body>
    <form action="/jumlah/vokal" method="POST" id="main-form">
        {{ csrf_field() }}
        <h1>Form Menghitung Jumlah Huruf Vokal</h1>
        <label for="teks">Kata atau kalimat:</label>
        <textarea name="teks" id="teks" cols="30" rows="10"></textarea>
        <button type="submit">SUBMIT</button>
    </form>
</body>

</html>