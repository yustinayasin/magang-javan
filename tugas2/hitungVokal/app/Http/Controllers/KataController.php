<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class KataController extends Controller
{
    //
    public function kalkulasi(Request $request)
    {
        $kata = filter_var($request->teks, FILTER_SANITIZE_STRING);
        $vokal = '@[aeiouAIUEO]@';

        preg_match_all($vokal, $kata, $matches);
        $unique = array_values(array_unique($matches[0]));

        $data = $kata . ": Ada " . count($unique) . " huruf vokal diantaranya ";
        for ($i = 0; $i < count($unique); $i++) {
            switch (count($unique)) {
                case 1:
                    $data = $data . $unique[$i];
                    break;
                case 2:
                    if ($i <= count($unique) - 2) {
                        $data = $data . $unique[$i];
                    } else {
                        $data = $data . " dan " . $unique[$i];
                    }
                    break;
                default:
                    if ($i <= count($unique) - 2) {
                        $data = $data . $unique[$i] . ", ";
                    } else {
                        $data = $data . " dan " . $unique[$i];
                    }
                    break;
            }
        }
        return View::make("tampilkan", compact('data'));
    }
}
