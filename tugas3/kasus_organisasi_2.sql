# mencari CEO
select nama from employee where atasan_id is null;

# mencari staff biasa
select nama from employee where id not in (select atasan_id from employee where atasan_id is not null);

# mencari direktur
select nama from employee where atasan_id=1;

#mencari manager
select nama from employee where atasan_id in (select id from employee where atasan_id=1);

# mencari jumlah bawahan pak budi
with recursive countUp as (
    select id as i
    from employee
    where atasan_id=1
    union all
    select id
    from countUp, employee
    where employee.atasan_id=countUp.i )
select count(*) from countUp;