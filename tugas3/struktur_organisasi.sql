/* select all staff */
select nama from staff;

/* select semua staff di bawah Farhan Reza */
select nama from staff where manager = 1;

/* select semua staff di bawah Riyandi Adi */
select nama from staff where manager = 2;