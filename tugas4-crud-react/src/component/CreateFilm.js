import React, { useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { Link } from "react-router-dom";
import '../css/CreateDaerah.css';

const CreateDaerah = () => {
    let edit = useLocation();
    const [judul, setJudul] = useState('');
    const [tiket, setTiket] = useState(null);
    const [isEditing, setIsEditing] = useState(edit["isEditingEdit"]);
    const [id] = useState(edit["id"]);

    const axios = require('axios').default;

    const handleSubmit = () => {
        axios({
            method: 'post',
            url: '/api/film',
            data: {
                judul: judul,
                tiket: tiket
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
    }

    const handleEdit = (id) => {
        axios({
            method: 'put',
            url: `/api/film/${id}`,
            data: {
                judul: judul,
                tiket: tiket
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
        setIsEditing(false);
    }

    return(
        <div className="card-create">
            <form onSubmit={isEditing ? () => handleEdit(id) : () => handleSubmit(id)}>
                <h1>Form Film</h1>
                <label htmlFor="judul">Judul:</label>
                <input type="text" 
                        className="judul" 
                        name="judul"
                        defaultValue={isEditing && edit["judulEdit"]}
                        placeholder="masukkan judul"
                        onChange={(e) => setJudul(e.target.value)}
                />
                <label htmlFor="tiket">Tiket:</label>
                <input type="number" 
                        className="tiket" 
                        name="tiket"
                        defaultValue={isEditing && edit["tiketEdit"]}
                        placeholder="masukkan tiket"
                        onChange={(e) => setTiket(e.target.value)}
                />
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">{isEditing ? 'Edit' : 'Submit'}</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateDaerah;