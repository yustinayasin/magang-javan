import React from 'react';
import '../css/ListDaerah.css';

const ListFilm = ({data, idFilm, removeItem}) => {
    return(
        <>
        {
            data.map((item, index) => {
                return (
                    <tr key={index}>
                        <td>{item.nama}</td>
                        <td>
                            <button onClick={() => removeItem(idFilm, item.id)} className="btn btn-delete">delete</button>
                        </td>
                    </tr>
                );
            })
        }
        </>
    );
}

export default ListFilm;