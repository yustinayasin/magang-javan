import React from 'react';
import { Link } from "react-router-dom";
import '../css/ListDaerah.css';

const ListDaerah = ({data, dataFilm, dataPenonton, removeItem, column, idDaerah}) => {
    const checkEdit = (idDaerah, item) => {
        if(idDaerah===1) {
            return <Link to={{
                pathname: "/edit/provinsi",
                id: item.id,
                namaEdit: item.nama,
                isEditingEdit: true,
            }}> <button className="btn btn-edit">edit</button>
            </Link>
        } else if(idDaerah===2) {
            return <Link to={{
                pathname: "/edit/kabupaten",
                id: item.id,
                namaEdit: item.nama,
                parent: item[column[2]],
                isEditingEdit: true,
            }}> <button className="btn btn-edit">edit</button>
            </Link>
        } else if (idDaerah===3) {
            return <Link to={{
                pathname: "/edit/kecamatan",
                id: item.id,
                namaEdit: item.nama,
                parent: item[column[2]],
                isEditingEdit: true,
            }}> <button className="btn btn-edit">edit</button>
            </Link>
        } else if (idDaerah===4) {
            return <Link to={{
                pathname: "/edit/desa",
                id: item.id,
                namaEdit: item.nama,
                parent: item[column[2]],
                isEditingEdit: true,
            }}> <button className="btn btn-edit">edit</button>
            </Link>
        } else if (idDaerah===5) {
            return (
            <>
            <Link to={{
                pathname: "/details/film",
                id: item.id,
                judulEdit: item.judul,
                tiketEdit: item.tiket,
                dataFilm: dataFilm,
                dataPenonton: dataPenonton,
                isEditingEdit: true
            }}> <button className="btn btn-details">details</button>
            </Link>
            <Link to={{
                pathname: "/edit/film",
                id: item.id,
                judulEdit: item.judul,
                tiketEdit: item.tiket,
                isEditingEdit: true,
            }}> <button className="btn btn-edit">edit</button>
            </Link>
            </>
            );
        } else {
            return <Link to={{
                pathname: "/edit/penonton",
                id: item.id,
                namaEdit: item.nama,
                isEditingEdit: true,
            }}> <button className="btn btn-edit">edit</button>
            </Link>
        }
    }
    return(
        <>
        {
            data.map((item, index) => {
                return (
                    <tr key={index}>
                        {
                            column.map((item2, index2) => {
                                return <td key={index2}>{item[item2]}</td>
                            })
                        }
                        <td>
                            {
                                checkEdit(idDaerah, item)
                            }
                            <button onClick={() => removeItem(item.id)} className="btn btn-delete">delete</button>
                        </td>
                    </tr>
                );
            })
        }
        </>
    );
}

export default ListDaerah;