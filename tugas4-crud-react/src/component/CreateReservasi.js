import React, {useState} from 'react';
import { Link, useLocation} from "react-router-dom";

const CreateReservasi = () => {
    const [filmId, setFilmId] = useState('');
    const [penontonId, setPenontonId] = useState('');
    let film = useLocation();

    const axios = require('axios').default;

    const handleSubmit = () => {
        axios({
            method: 'post',
            url: `/api/reservasi`,
            data: {
                film_id: filmId,
                penonton_id: penontonId,
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
    }

    return(
        <div className="card-create">
            <form onSubmit={() => handleSubmit()}>
                <h1>Form Reservasi</h1>
                <label htmlFor="nama">Film ID:</label>
                <select name="idMatkul" className="idMatkul" onChange={(e) => setFilmId(e.target.value)}>
                    {
                        film.dataFilm ? 
                        <>
                            {
                                film.dataFilm.map((item, index) => {
                                    return <option key={index} value={item.id}>{item.judul}</option>
                                })
                            }
                        </>
                        :
                        "Tidak ada film"
                    }
                </select>
                <label htmlFor="judul">Penonton ID:</label>
                <select name="idMatkul" className="idMatkul" onChange={(e) => setPenontonId(e.target.value)}>
                    {
                        film.dataPenonton ? 
                        <>
                            {
                                film.dataPenonton.map((item, index) => {
                                    return <option key={index} value={item.id}>{item.nama}</option>
                                })
                            }
                        </>
                        :
                        "Tidak ada penonton"
                    }
                </select>
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">Submit</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateReservasi;