import React from 'react';
import { Link } from "react-router-dom";
import '../css/Daerah.css';

const Daerah = () => {
    return(
        <div className="daerah">
            <Link to={{
                    pathname: "/index/provinsi",
                    idPage: 1
            }}>
                <button className="btn btn-provinsi">Provinsi</button>
            </Link>
            <Link to={{
                    pathname: "/index/kabupaten",
                    idPage: 2
            }}>
                <button className="btn btn-kabupaten">Kabupaten</button>
            </Link>
            <Link to={{
                    pathname: "/index/kecamatan",
                    idPage: 3
            }}>
                <button className="btn btn-kecamatan">Kecamatan</button>
            </Link>
            <Link to={{
                    pathname: "/index/desa",
                    idPage: 4
            }}>
                <button className="btn btn-desa">Desa</button>
            </Link>
            <Link to={{
                    pathname: "/film",
                    id: 2,
                    idPage: 5
            }}>
                <button className="btn btn-film">Film</button>
            </Link>
        </div>
    );
}

export default Daerah;