import React, { useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { Link } from "react-router-dom";

const CreateKecamatan = () => {
    let edit = useLocation();
    const [nama, setNama] = useState('');
    const [kabupaten, setKabupaten] = useState(null);
    const [isEditing, setIsEditing] = useState(edit["isEditingEdit"]);
    const [id] = useState(edit["id"]);

    const axios = require('axios').default;

    useEffect(() => {
        if(edit.namaEdit && isEditing) {
            setNama(edit["namaEdit"]);
            setKabupaten(edit["parent"]);
        }  
    }, []);


    const handleSubmit = () => {
        axios({
            method: 'post',
            url: '/api/kecamatan',
            data: {
                nama: nama,
                kabupaten_id: kabupaten
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
    }

    const handleEdit = (id) => {
        axios({
            method: 'put',
            url: `/api/kecamatan/${id}`,
            data: {
                nama: nama,
                kabupaten_id: kabupaten
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
        setIsEditing(false);
    }

    return(
        <div className="card-create">
            <form onSubmit={isEditing ? () => handleEdit(id) : () => handleSubmit(id)}>
                <h1>Form Kecamatan</h1>
                <label htmlFor="nama">Nama</label>
                <input type="text" 
                        className="nama" 
                        name="nama"
                        defaultValue={isEditing && edit["namaEdit"]}
                        placeholder="masukkan nama"
                        onChange={(e) => setNama(e.target.value)}
                />
                <label htmlFor="kabupaten_id">Kabupaten ID</label>
                <input type="number" 
                        className="kabupaten-id" 
                        name="kabupaten_id"
                        defaultValue={isEditing && edit["parent"]}
                        placeholder="masukkan id kabupaten"
                        onChange={(e) => setKabupaten(e.target.value)}
                />
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">{isEditing ? 'Edit' : 'Submit'}</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateKecamatan;