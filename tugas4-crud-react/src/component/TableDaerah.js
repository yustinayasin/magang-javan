import React, { useEffect, useState} from 'react';
import ListDaerah from './ListDaerah';
import { Link, useLocation } from "react-router-dom";
import '../css/TableDaerah.css';

const TableDaerah = () => {
    let daerah = useLocation();
    const [data, setData] = useState([]);
    const [column, setColumn] = useState([]);
    const [url, setUrl] = useState('');
    const [idDaerah] = useState(daerah.idPage);
    const [namaJudul, setNamaJudul] = useState('');

    useEffect(() => {
        if(idDaerah===1){
            setColumn(['id', 'nama', 'aksi']);
            setUrl('/provinsi');
            setNamaJudul('Provinsi');
        } else if(idDaerah===2) {
            setColumn(['id', 'nama', 'provinsi_id', 'aksi']);
            setUrl('/kabupaten');
            setNamaJudul('Kabupaten');
        } else if(idDaerah===3) {
            setColumn(['id', 'nama', 'kabupaten_id', 'aksi']);
            setUrl('/kecamatan');
            setNamaJudul('Kecamatan');
        } else {
            setColumn(['id', 'nama', 'kecamatan_id', 'aksi']);
            setUrl('/desa');
            setNamaJudul('Desa');
        }
    }, []);

    useEffect(() => {
        async function fetchMyAPI() {
            await fetch('/api'+url, {
                method: 'GET'
            }).then(response => {
                if(!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(response => {
                if(response.Response === 'False') {
                    throw new Error(response.Error);
                }
                setData(response.data);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        if(url) {
            fetchMyAPI();
        }
    }, [url]);

    const handleDelete = (id) => {
        fetch(`/api${url}/${id}`, {
            method: 'DELETE'
        })
        .catch(error => {
            console.log("Error fetching data: ", error);
        });
    }

    return (
        <div className="daerah-card">
            <h1>Daftar {namaJudul}</h1>
            <table>
                <thead>
                    <tr>
                        {
                            column.map((item, index) => {
                                return <th key={index}>{item}</th>
                            })
                        }
                    </tr>
                </thead>
                <tbody>
                    {
                        data.length > 0 &&
                        <ListDaerah data={data} idDaerah={idDaerah} removeItem={handleDelete} column={column.slice(0, column.length-1)}/>
                    }
                </tbody>
            </table>
            <div className="btn-wrapper">
                <Link to={{
                    pathname: "/add"+url
                }}>
                    <button className="btn btn-add">Tambah</button>
                </Link>
                <Link to='/'><button className="btn btn-back">Back</button></Link>
            </div>
        </div>
    );
}

export default TableDaerah;