import React, { useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import { Link } from "react-router-dom";

const CreateDesa = () => {
    let edit = useLocation();
    const [nama, setNama] = useState('');
    const [kecamatan, setKecamatan] = useState(null);
    const [isEditing, setIsEditing] = useState(edit["isEditingEdit"]);
    const [id] = useState(edit["id"]);

    const axios = require('axios').default;

    useEffect(() => {
        if(edit.namaEdit && isEditing) {
            setNama(edit["namaEdit"]);
            setKecamatan(edit["parent"]);
        }  
    }, []);


    const handleSubmit = () => {
        axios({
            method: 'post',
            url: '/api/desa',
            data: {
                nama: nama,
                kecamatan_id: kecamatan
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
    }

    const handleEdit = (id) => {
        axios({
            method: 'put',
            url: `/api/desa/${id}`,
            data: {
                nama: nama,
                kecamatan_id: kecamatan
            }
        }).then(response => console.log(response))
        .catch(err => console.error(err));
        setIsEditing(false);
    }

    return(
        <div className="card-create">
            <form onSubmit={isEditing ? () => handleEdit(id) : () => handleSubmit(id)}>
                <h1>Form Desa</h1>
                <label htmlFor="nama">Nama</label>
                <input type="text" 
                        className="nama" 
                        name="nama"
                        defaultValue={isEditing && edit["namaEdit"]}
                        placeholder="masukkan nama"
                        onChange={(e) => setNama(e.target.value)}
                />
                <label htmlFor="kecamatan_id">Kecamatan ID</label>
                <input type="number" 
                        className="kecamatan-id" 
                        name="kecamatan_id"
                        defaultValue={isEditing && edit["parent"]}
                        placeholder="masukkan id kecamatan"
                        onChange={(e) => setKecamatan(e.target.value)}
                />
                <div className="btn-wrapper">
                    <button className="btn btn-add" type="submit">{isEditing ? 'Edit' : 'Submit'}</button>
                    <Link to="/"><button className="btn btn-home">Home</button></Link>
                </div>
            </form>
        </div>
    );
}

export default CreateDesa;