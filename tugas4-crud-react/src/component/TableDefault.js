import React, { useEffect, useState} from 'react';
import ListDaerah from './ListDaerah';
import { Link, useLocation } from "react-router-dom";
import '../css/TableDaerah.css';

const TableDefault = () => {
    let daerah = useLocation();
    const [data, setData] = useState([]);
    const [column, setColumn] = useState([]);
    const [url, setUrl] = useState('');
    const [dataFilm, setDataFilm] = useState([]);
    const [dataPenonton, setDataPenonton] = useState([]);
    const [idData] = useState(daerah.id);
    const [namaJudul, setNamaJudul] = useState('');

    useEffect(() => {
        if(idData===1){
            setColumn(['id', 'nama', 'aksi']);
            setUrl('/penonton');
            setNamaJudul('Penonton');
        } else {
            setColumn(['id', 'judul', 'tiket', 'aksi']);
            setUrl('/film');
            setNamaJudul('Film');
        }
    }, []);

    useEffect(() => {
        async function fetchMyAPI() {
            fetch('/api'+url, {
                method: 'GET'
            }).then(response => {
                if(!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(response => {
                if(response.Response === 'False') {
                    throw new Error(response.Error);
                }
                setData(response.data);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        if(url) {
            fetchMyAPI();
        }
    }, [url]);

    useEffect(() => {
        async function fetchMyAPI() {
            await fetch('/api/film', {
                method: 'GET'
            }).then(response => {
                if(!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(response => {
                if(response.Response === 'False') {
                    throw new Error(response.Error);
                }
                setDataFilm(response.data);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        fetchMyAPI();
    });

    useEffect(() => {
        async function fetchMyAPI() {
            await fetch('/api/penonton', {
                method: 'GET'
            }).then(response => {
                if(!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(response => {
                if(response.Response === 'False') {
                    throw new Error(response.Error);
                }
                setDataPenonton(response.data);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        fetchMyAPI();
    });
    

    const handleDelete = (idFilm, idPenonton) => {
        fetch(`/api/batal-reservasi`, {
            method: 'POST',
            data: {
                film_id: idFilm,
                penonton_id: idPenonton
            }
        })
        .catch(error => {
            console.log("Error fetching data: ", error.response.data);
        });
    }

    return (
        <div className="daerah-card">
            <h1>Daftar {namaJudul}</h1>
            <table>
                <thead>
                    <tr>
                        {
                            column.map((item, index) => {
                                return <th key={index}>{item}</th>
                            })
                        }
                    </tr>
                </thead>
                <tbody>
                    {
                        data.length > 0 &&
                        <ListDaerah dataFilm={dataFilm} dataPenonton={dataPenonton} data={data} idData={idData} idDaerah={idData===1 ? 6 : 5} removeItem={handleDelete} column={column.slice(0, column.length-1)}/>
                    }
                </tbody>
            </table>
            <div className="btn-wrapper">
                <Link to={{
                    pathname: "/add"+url
                }}>
                    <button className="btn btn-add">Tambah</button>
                </Link>
                <Link to='/'><button className="btn btn-back">Back</button></Link>
                {
                    daerah.idPage===5 &&
                    <Link to={{
                        pathname: "/penonton",
                        id: 1,
                        idDaerah: 6
                    }}>
                        <button className="btn btn-penonton">Table Penonton</button>
                    </Link>
                }
            </div>
        </div>
    );
}

export default TableDefault;