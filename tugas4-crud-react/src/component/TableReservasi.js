import React, { useEffect, useState} from 'react';
import ListFilm from './ListFilm';
import { Link, useLocation  } from "react-router-dom";
import '../css/TableDaerah.css';

const TableReservasi = () => {
    let film = useLocation();
    const [reservasi, setReservasi] = useState([]);

    useEffect(() => {
        async function fetchMyAPI() {
            await fetch(`/api/film/${film.id}`, {
                method: 'GET'
            }).then(response => {
                if(!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(response => {
                if(response.Response === 'False') {
                    throw new Error(response.Error);
                }
                setReservasi(response.reservasi);
            }).catch(error => {
                console.log("Error fetching data: ", error);
            });
        }
        fetchMyAPI();
    }, []);

    const handleDelete = (idPenonton, idFilm) => {
        fetch(`/api/film/${idPenonton}/${idFilm}`, {
            method: 'DELETE'
        })
        .catch(error => {
            console.log("Error fetching data: ", error);
        });
    }

    return (
        <div className="film-card">
            <h1>Daftar Reservasi Film</h1>
            <table>
                <thead>
                    <tr>
                        <th>Nama Penonton</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                {
                    reservasi.length > 0 ? 
                    <tbody>
                        <ListFilm data={reservasi} idFilm={film.id} removeItem={handleDelete}/>
                    </tbody> :
                    <p>Tidak ada reservasi</p>
                }
            </table>
            <div className="btn-wrapper">
                <Link to={{
                    pathname: "/add/reservasi",
                    dataFilm: film.dataFilm,
                    dataPenonton: film.dataPenonton
                }}>
                    <button className="btn btn-add">Tambah</button>
                </Link>
                <Link to='/'><button className="btn btn-add">Back</button></Link>
            </div>
        </div>
    );
}

export default TableReservasi;