import { BrowserRouter as Router, withRouter, Route, Switch } from "react-router-dom";
import './App.css';
import Daerah from './component/Daerah';
import TableDaerah from './component/TableDaerah';
import TableReservasi from './component/TableReservasi';
import TableDefault from './component/TableDefault';
import CreateDaerah from './component/CreateDaerah';
import CreateKabupaten from './component/CreateKabupaten';
import CreateKecamatan from './component/CreateKecamatan';
import CreateDesa from './component/CreateDesa';
import CreateFilm from './component/CreateFilm';
import CreatePenonton from './component/CreatePenonton';
import CreateReservasi from './component/CreateReservasi';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Daerah} />
        <Route path="/index/reservasi" exact component={withRouter(TableReservasi)} />
        <Route path="/index/:daerah" exact component={withRouter(TableDaerah)} />
        <Route path="/add/provinsi" exact component={withRouter(CreateDaerah)} />
        <Route path="/add/kabupaten" exact component={withRouter(CreateKabupaten)} />
        <Route path="/add/kecamatan" exact component={withRouter(CreateKecamatan)} />
        <Route path="/add/desa" exact component={withRouter(CreateDesa)} />
        <Route path="/add/reservasi" exact component={withRouter(CreateReservasi)} />
        <Route path="/edit/provinsi" exact component={withRouter(CreateDaerah)} />
        <Route path="/edit/kabupaten" exact component={withRouter(CreateKabupaten)} />
        <Route path="/edit/kecamatan" exact component={withRouter(CreateKecamatan)} />
        <Route path="/edit/desa" exact component={withRouter(CreateDesa)} />
        <Route path="/film" exact component={withRouter(TableDefault)} />
        <Route path="/add/film" exact component={withRouter(CreateFilm)} />
        <Route path="/add/reservasi" exact component={withRouter(CreateReservasi)} />
        <Route path="/edit/film" exact component={withRouter(CreateFilm)} />
        <Route path="/penonton" exact component={withRouter(TableDefault)} />
        <Route path="/add/penonton" exact component={withRouter(CreatePenonton)} />
        <Route path="/edit/penonton" exact component={withRouter(CreatePenonton)} />
        <Route path="/details/film" exact component={withRouter(TableReservasi)} />
      </Switch>
  </Router>
  );
}

export default App;
