CREATE TABLE customers (
    customersid varchar,
    firstname varchar,
    surname varchar,
    PRIMARY KEY(customersid)
);

CREATE TABLE items (
    itemid varchar,
    description varchar,
    retail_price float(2),
    PRIMARY KEY(itemid)
);

CREATE TABLE transactions (
    transactionid varchar,
    timestampsec timestamp,
    customerid varchar,
    shipping_state varchar,
    loyalty_discount float(2),
    PRIMARY KEY(transactionid),
    CONSTRAINT customerid
	FOREIGN KEY (customerid)
		REFERENCES customers(customersid)
);

CREATE TABLE transactionsItems (
    transactionid varchar,
    itemid varchar,
    CONSTRAINT transactionid
   FOREIGN KEY (transactionid)
      REFERENCES transactions(transactionid),
    CONSTRAINT itemid
   FOREIGN KEY (itemid)
      REFERENCES items(itemid),
    PRIMARY KEY(transactionid, itemid)
);

--COPY transactions FROM 'E:\Belajar\Resource Magang Javan\Transactions.csv' DELIMITER ',' CSV HEADER;

--COPY items FROM 'E:\Belajar\Resource Magang Javan\Items.csv' DELIMITER ',' CSV HEADER;

--COPY customers FROM 'E:\Belajar\Resource Magang Javan\Customers.csv' DELIMITER ',' CSV HEADER;

--COPY transactionsitems FROM 'E:\Belajar\Resource Magang Javan\TransactionsItems.csv' DELIMITER ',' CSV HEADER;
