--count complaint every month
select count(*)
from consumercomplaints
group by month(`Date Received`);

select *
from consumercomplaints
where Tags = 'Older American';

select
    company,
    SUM(CASE
        WHEN `Company Response to Consumer`='Closed' THEN 1
        ELSE 0
    END) as `Closed`,
    SUM(CASE
        WHEN `Company Response to Consumer`='Closed with explanation' THEN 1
        ELSE 0
    END) as `Closed with explanation`,
    SUM(CASE
        WHEN `Company Response to Consumer`='Closed with non-monetary relief' THEN 1
        ELSE 0
    END) as `Closed with non-monetary relief`
from consumercomplaints
where `Company Response to Consumer` in ('Closed', 'Closed with explanation', 'Closed with non-monetary relief')
group by company