-- pets name and owner name
select pets.name , owners.name ||' '|| owners.surname AS Owner
from pets
join owners on pets.ownerid=owners.ownerid;

-- find out which pets from this clinic had procedures performed
select pets.name
from pets, procedurehistory
where pets.petid = procedurehistory.petid;

-- match up all procedures perfomed to their descriptions
select procedurehistory.petid, procedurehistory.proceduredate, procedurehistory.proceduretype, procedurehistory.proceduresubcode, p.description
from procedurehistory
join proceduredetails p on procedurehistory.proceduresubcode = p.proceduresubcode and procedurehistory.proceduresubcode=p.proceduresubcode;

-- same as above but only or pets from the clinic in question
select procedurehistory.petid, procedurehistory.proceduredate, procedurehistory.proceduretype, procedurehistory.proceduresubcode, p.description
from procedurehistory
join proceduredetails p
    on procedurehistory.proceduresubcode = p.proceduresubcode and procedurehistory.proceduresubcode=p.proceduresubcode
where procedurehistory.petid in (select petid from pets);

-- extract individual cost
SELECT MAX(ow.name ||' '|| ow.surname) AS Owner
     , SUM(pd.price) as price
FROM owners ow
INNER JOIN pets p
        ON ow.ownerid = p.ownerid
INNER JOIN procedurehistory p2
        ON p.petid = p2.petid
INNER JOIN proceduredetails pd
        ON pd.proceduretype = p2.proceduretype
GROUP BY ow.ownerid;
