-- menghitung seberapa % penjualan NA dibanding penjualan global
SELECT sum(na_sales)/sum(na_sales+eu_sales+jp_sales+other_sales)*100
FROM console_games;

-- menampilkan judul game yang dirututkan berdasarkan nama platform & tahun
select game_name
from console_games
order by platform, game_year DESC;

-- 4 huruf pertama publisher
select substring(publisher, 1, 4)
from console_games;

-- console game yang dirilis sebelum black friday atau sebelum natal
select platform_name
from console_dates
where (EXTRACT(MONTH FROM first_retail_availability) = 12 and EXTRACT(DAY FROM first_retail_availability) = 24)
or (EXTRACT(MONTH FROM first_retail_availability) = 10 and extract(day from first_retail_availability)=17);

-- select longest period of a platform
select platform_name
from console_dates
order by age(discontinued, first_retail_availability);

-- mengubah tipe data kolom game_year
ALTER TABLE console_games
        ALTER COLUMN game_year TYPE VARCHAR(50) USING game_year::varchar;

-- how to deal with missing data in the file
-- menggunakan atribute FORCE_NOT_NULL ketika import csv file
-- COPY console_games FROM 'C:\Users\Kirill\Desktop\Databases\ConsoleGames.csv' DELIMITER ',' CSV HEADER FORCE NOT NULL ;